# coding=utf8
from irbis32 import *
import csv

fi = open('db\svk.mst', 'rb')
fo = open('db\svk.csv', 'wb')

db = irbis32_read(fi, 'cp1251')

fcsv = csv.writer(fo, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

'''
    ID                  str(i)
    TYPE                PAZK / IBIS || SPEC
    Название            ['200'][0]['A'] || ['461'][0]['C']
    Автор               ['200'][0]['F'] || ['461'][0]['F']
    Издательство        ['210'][0]['С'] || ['461'][0]['G']
    Год                 ['210'][0]['D'] || ['461'][0]['H']
    Кол-во страниц      ['215'][0]['A']
    
    ISBN                ['10'][0]['A']
    ключевые слова      ['610'][?]
'''
for i in db.Index():
    i_row = db.Read(i)
    data = []
    
    if (not i_row['success']):
        continue
    
    # ID
    data.append(str(i))
    
    # TYPE
    type = db.ReadField(i_row, '920')
    if (type != None):
        type = type[0]
    else:
        type = 'PAZK'
    data.append(type)
    
    # Название
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'C')
    else:
        r = db.ReadField(i_row, '200', 'A')
    if (r != None):
        data.append(r)
        if (type == 'SPEC'):
            r = db.ReadField(i_row, '200', 'V')
            if (r != None):
                data[ len(data)-1 ] += ' ' + r
            r = db.ReadField(i_row, '200', 'A')
            if (r != None):
                data[ len(data)-1 ] += ' ' + r
    else:
        r = ''
        data.append(r)
    
    # Автор
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'F')
    else:
        r = db.ReadField(i_row, '200', 'F')
    if (r == None):
        r = ''
    data.append(r)
    
    # Издательство
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'G')    
    else:
        r = db.ReadField(i_row, '210', 'C')
    if (r == None):
        r = ''
    data.append(r)
    
    # Год
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'H')    
    else:
        r = db.ReadField(i_row, '210', 'D')
    if (r == None):
        r = ''
    data.append(r)
    
    # Кол-во страниц
    r = db.ReadField(i_row, '215', 'A')
    if (r != None):
        r = re.search('([0-9]+)', r)
        if (r != None):
            r = r.group(0)
    if (r == None):
        r = ''
    data.append(r)
    
    # ISBN
    r = db.ReadField(i_row, '10', 'A')
    if (r == None):
        r = ''
    data.append(r)
    
    for j in range(0,len(data)):
        data[j] = data[j].encode('utf-8')
    
    fcsv.writerow(data)

fi.close()
fo.close()