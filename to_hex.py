import binascii

filename = 'db\svk.mst'
output = 'db\svk.txt'

with open(filename, 'rb') as f:
    hex = binascii.hexlify(f.read()).upper()

with open(output, 'w') as f:
    f.write(' '.join(hex[i:i+2] for i in range(0, len(hex), 2)))