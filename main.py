# coding=utf8
import re, sqlite3, math
from sqlite3_helpers import *

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.carousel import Carousel
from kivy.factory import Factory
from kivy.uix.spinner import Spinner
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from kivy.graphics import Color, Rectangle

fi = sqlite3.connect('db\main.sq3')
cur = fi.cursor()

fi.create_collation("BINARY", sqlite_nocase_collation)
fi.create_collation("NOCASE", sqlite_nocase_collation)

fi.create_function("LIKE", 2, sqlite_like)
fi.create_function("LOWER", 1, sqlite_lower)
fi.create_function("UPPER", 1, sqlite_upper)

aaa = '''
Label:
    id: result_none
    text: 'По запросу ничего не найдено.'
    size_hint_y: None
    height: 150
    text_size: self.width, None
    halign: 'center'
    markup: True
'''

aab = '''
<resultObj>:
    orientation: 'vertical'
    size_hint_y: None
    height: 50
    #padding_y: 50
    Label:
        id: title
        text: 'Название1'
        text_size: self.width, None
        halign: 'left'
        markup: True
        shorten: True
        shorten_from: 'right'
        # on_ref_press:
        #     print '-'*10 + '>'
        #     print root.id
    Label:
        id: author
        text: 'Название2'
        text_size: self.width, None
        halign: 'left'
        markup: True
        shorten: True
        shorten_from: 'right'
    Label:
        id: year
        text: 'Название3'
        text_size: self.width, None
        halign: 'left'
        markup: True
        shorten: True
        shorten_from: 'right'
'''

aac = '''
Label:
    text: 'Заголовок'
    size_hint_y: None
    height: 100
    text_size: self.size
    halign: 'left'
    valign: 'top'
    shorten_from: 'right'
    markup: True
    
    #size_hint: None, None
    #size: self.texture_size
    # canvas.before:
    #     Color:
    #         rgb: 0, 1, 0
    #     Rectangle:
    #         pos: self.pos
    #         size: self.size
'''

Builder.load_string(aab)

class resultObj(BoxLayout):
    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            return
        
        j = int(self.id.replace('id-', ''))
        j = rapp.data['result'][j]
        
        o = rapp.ids.details_output
        o.clear_widgets()
        
        w = Builder.load_string(aac)
        w.text = u'[b]Название:[/b] {0}'.format(j[2])
        o.add_widget(w)
        
        w = Builder.load_string(aac)
        w.text = u'[b]Автор:[/b] {0}'.format(j[3] if len(j[3]) else '-')
        w.height = 20
        w.shorten = True
        o.add_widget(w)
        
        w = Builder.load_string(aac)
        w.text = u'[b]Издательство:[/b] {0}'.format(j[4] if len(j[4]) else '-')
        w.height = 20
        w.shorten = True
        o.add_widget(w)
        
        w = Builder.load_string(aac)
        w.text = u'[b]Год:[/b] {0}'.format(j[5] if len(j[5]) else '-')
        w.height = 20
        w.shorten = True
        o.add_widget(w)
        
        w = Builder.load_string(aac)
        w.text = u'[b]Страниц:[/b] {0}'.format(j[6] if len(j[6]) else '-')
        w.height = 20
        w.shorten = True
        o.add_widget(w)
        
        w = Builder.load_string(aac)
        w.text = u'[b]ISBN:[/b] {0}'.format(j[7] if len(j[7]) else '-')
        w.height = 20
        w.shorten = True
        o.add_widget(w)
        
        w = Builder.load_string(aac) 
        dt = j[8].strip().replace(',', ' ').replace(u'директор методический отдел', u'м.о.').split(' ') if len(j[8]) else []
        ma = ''
        mb = ''
        for i in dt:
            if i.isdigit() or re.match('(\d+\-\d+)', i) or re.match('\d+\w+', i, re.U):
                mb += (', ' if mb else '') + i
            elif len(i) == 0:
                continue
            else:
                i = i.replace(u'аб', u'абонемент')\
                    .replace(u'чз', u'читальный зал')\
                    .replace(u'кх', u'книгохранение')
                i = re.sub(u'(?ui)^м(\.|)о(\.|)$', u'методический отдел', i)
                ma += (', ' if ma else '') + i
        w.text = u'[b]Где найти:[/b]{0}{1}'.format(
                u'\n- [i]Библиотека им. Ю.Н. Либединского:[/i] ' + ma if len(ma) else '',
                u'\n- [i]Филиалы:[/i] ' + mb if len(mb) else '')
        o.add_widget(w)
        
        rapp.ids.scr_mngr.current = 'details'
                
    # pass

class rootObj(RelativeLayout):
    columns = [
        'Всему',
        'Названию',
        'Автору',
        'Издательству',
        'Году',
        'Кол-ву страниц',
        'ISBN']
    
    columns_t = [
        'title',
        'author',
        'publisher',
        'year',
        'pages',
        'isbn']
    
    data = {'current': 1, 'limit': 10, 'loaded': False}
    
    def loadpage(self, page = None):
        if(not self.data['loaded']):
            return
        self.data['loaded'] = False
        
        if (page == None):
            page = self.data['current']
        else:
            self.data['current'] = page
        page -= 1
        
        cur.execute(self.data['query'], (page*self.data['limit'], self.data['limit']))
        self.data['result'] = cur.fetchall()

        o = self.ids.results_output
        o.clear_widgets()
        
        j = 0
        for i in self.data['result']:
            w = resultObj()
            w.id = u'id-{0}'.format(j)
            w.ids.title.text = u'[b]{0}[/b]'.format(i[2])
            w.ids.author.text = u'Автор: {0}'.format(i[3] if len(i[3]) else '-')
            w.ids.year.text = u'Год: {0}'.format(i[5] if len(i[5]) else '-')
            o.add_widget(w)
            j += 1   
                
        self.data['loaded'] = True
        
        p = self.ids.pagination_back
        p.text = '<' if page else '' 
             
        p = self.ids.pagination_next
        p.text = '>' if self.data['count'] != page else ''
        
        if self.data['count'] > 0:
            left = page - 1
            start = 0
            if left >= math.floor(5 / 2):
                start = page - int(math.floor(5 / 2))
            end = start + 5 - 1
            if end > self.data['count']:
                start -= end - self.data['count']
                end = self.data['count']
                if start < 0:
                    start = 0
            for i in range(1, self.data['pagination']):
                p = self.ids['pagination_'+str(i)]
                p.text = str(start+i)
                if page == start+i-1:
                    c = p
            p = self.ids.results_pagination
            p.canvas.before.remove_group('curpage')
            with p.canvas.before:
                Color(245 / 255.0, 246 / 255.0, 206 / 255.0, .2, group='curpage')
                Rectangle(size=(50, 50), group='curpage', pos=(c.x,0))

    # I'm so sorry :/
    def notfound(self):
        self.data['loaded'] = False
        
        o = self.ids.results_output
        o.clear_widgets()
        
        w = Builder.load_string(aaa)
        w.text = u'По запросу [b]{0}[/b] ничего не найдено.'.format(self.ids.search_box.text)
        o.add_widget(w)
        
    
    def search(self):
        query = self.search_query(self.search_parse(self.ids.search_box.text))
        if(not query):
            return
        
        cur.execute('SELECT Count(*) ' + query)
        self.data['count'] = cur.fetchone()[0]
        self.ids.lbl1.text = 'Поиск - найдено: {0}'.format(self.data['count'])
        
        if(self.data['count'] == 0):
            self.notfound()
            return
        
        self.data['current'] = 1
        self.data['loaded'] = True
        self.data['pagination'] = 1
        self.data['query'] = 'SELECT * ' + query + ' LIMIT ?,?'        
        self.data['count'] = (self.data['count'] / 10) + (1 if self.data['count'] % 10 else 0) - 1
        
        for i in range(1,6):
            self.ids['pagination_'+str(i)].text = '' if(self.data['count'] < i-1) else str(i)
            self.ids['pagination_'+str(i)].width = 0 if(self.data['count'] < i-1) else 50
            self.data['pagination'] += 0 if(self.data['count'] < i-1) else 1
        
        self.loadpage()
    
    def search_parse(self, string):
        # "ищем фразы" в строке
        data = re.findall('"(.*?)"', string)
        for i in data:
            string = string.replace('"' + i + '"', '', 1)
        string = string.strip().replace('  ', ' ').replace('"', '')
        # разбиваем на отдельные слова
        if(len(string) > 0):
            data += string.split(' ')
        return data
        
    def search_query(self, data):
        if(len(data) == 0):
            return False
        
        inall = (self.ids.spr1.text == self.columns[0])
        bycols = ''
        for i in range(1, len(self.columns)):
            if(inall or self.ids.spr1.text == self.columns[i]):
                bycols += (' || ' if len(bycols) else '') + self.columns_t[i-1]
        
        query = ''
        for i in data:
            if(len(query) > 0):
                query += ' OR'
            #query += u' (title || author || publisher || year || pages || isbn) LIKE "%{0}%"'.format(i)
            query += u' ({0}) LIKE "%{1}%"'.format(bycols, i)
        query = 'FROM `svk` WHERE' + query
        return query

rapp = None

class MainApp(App):
    def build(self):
        __main__.rapp = rootObj()
        return rapp

    def on_pause(self):
        return True
        
    def on_stop(self):
        fi.close()

MainApp().run()  