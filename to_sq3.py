# coding=utf8
from irbis32 import *
import sqlite3

fi = open('db\svk.mst', 'rb')
fo = sqlite3.connect('db\main.sq3')

db = irbis32_read(fi, 'cp1251')
cur = fo.cursor()

#cur.execute('DROP TABLE IF EXISTS `svk`')
cur.execute('''CREATE TABLE IF NOT EXISTS `svk` (
                id INTEGER PRIMARY KEY,
                type VARCHAR(16),
                title TEXT,
                author TEXT,
                publisher TEXT,
                year TEXT,
                pages TEXT,
                isbn TEXT,
                wfind TEXT
            )''')
cur.execute('DELETE FROM `svk`')
fo.commit()

for i in db.Index():
    i_row = db.Read(i)
    data = []
    
    if (not i_row['success']):
        continue
    
    # ID
    data.append(str(i))
    
    # TYPE
    type = db.ReadField(i_row, '920')
    if (type != None):
        type = type[0]
    else:
        type = 'PAZK'
    data.append(type)
    
    # Название
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'C')
    else:
        r = db.ReadField(i_row, '200', 'A')
    if (r != None):
        data.append(r)
        if (type == 'SPEC'):
            r = db.ReadField(i_row, '200', 'V')
            if (r != None):
                data[ len(data)-1 ] += ' ' + r
            r = db.ReadField(i_row, '200', 'A')
            if (r != None):
                data[ len(data)-1 ] += ' ' + r
    else:
        r = ''
        data.append(r)
    
    # Автор
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'F')
    else:
        r = db.ReadField(i_row, '200', 'F')
    if (r == None):
        r = ''
    data.append(r)
    
    # Издательство
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'G')    
    else:
        r = db.ReadField(i_row, '210', 'C')
    if (r == None):
        r = ''
    data.append(r)
    
    # Год
    if (type == 'SPEC'):
        r = db.ReadField(i_row, '461', 'H')    
    else:
        r = db.ReadField(i_row, '210', 'D')
    if (r == None):
        r = ''
    data.append(r)
    
    # Кол-во страниц
    r = db.ReadField(i_row, '215', 'A')
    '''
    if (r != None):
        r = re.search('([0-9]+)', r)
        if (r != None):
            r = r.group(0)
    '''
    if (r == None):
        r = ''
    data.append(r)
    
    # ISBN
    r = db.ReadField(i_row, '10', 'A')
    if (r == None):
        r = ''
    data.append(r)
    
    # Где можно найти
    r = db.ReadField(i_row, '910', 'D')
    if (r == None):
        r = ''
    else:
        r = r.replace('!', '1')
    data.append(r)
    
    cur.execute("INSERT INTO `svk` VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", data)
fo.commit()

fi.close()
fo.close()