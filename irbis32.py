# coding=utf8
# https://docs.python.org/2/library/ctypes.html#fundamental-data-types
# https://docs.python.org/2/library/struct.html#format-characters

import os, re
import struct
# from ctypes import *

# .decode(self.m_sEncoding)

class irbis32_read(object):
    def __init__(self, hFile, sEnconding = 'utf-8'):
        self.m_hFile = hFile
        self.m_sEncoding = sEnconding
        self.m_nDataBytes = os.fstat(hFile.fileno()).st_size
        
        print "[LOG] Start of reading..."
        
        hFile.seek(0)
        '''
            0 - CTLMFN
            1 - NXTMFN
            2 - ???
            3 - ???
        '''
        # self.m_aHeader = struct.unpack('<llll', self.m_pFile.read(16))
        hFile.seek(64, 1)
        
        self.m_aIndex = {}
        self.m_iRecords = [0, 0]
        offset = 0
        
        while (hFile.tell() != self.m_nDataBytes):
            # print "[LOG] {0:.1%}...".format( float(hFile.tell())/float(self.m_nDataBytes) )
            self.m_iRecords[0] += 1
            
            offset = hFile.tell() % 512
            offset = (512 - offset) if (499 < offset and offset < 511) else 0
            
            hFile.seek(offset, 1)
            
            '''
                0 - MFN (long)
                1 - MFRL (short)
                2 - ??? (short)
                3 - ??? (short)
                4 - ??? (short)
                5 - BASE (short)
                6 - NVF (short)
                7 - ??? (short)
            '''
            h = struct.unpack('<lhhhhhhh', hFile.read(18))
            
            # Записи закончились
            if (h[0] == 0):
                break
            # Меток нет
            elif (h[6] == 0):
                continue
            
            hFile.read(6*h[6])
            hFile.read(abs(h[1])-h[5])
            
            if (h[0] not in self.m_aIndex):
                self.m_aIndex[ h[0] ] = {}
            if (('start' not in self.m_aIndex[ h[0] ]) or (self.m_aIndex[ h[0] ]['mfrl'] < 0)):
                self.m_aIndex[ h[0] ]['start'] = hFile.tell() - abs(h[1])
                self.m_aIndex[ h[0] ]['mfrl'] = h[1]
        
        self.m_iRecords[1] = len(self.m_aIndex)
        
        print "[LOG] End of reading..."
        
    def Close(self):
        if (self.m_hFile):
            self.m_hFile.close()
            self.m_hFile = None
    
    def Index(self):
        return self.m_aIndex
    
    def Read(self, id, wstr = True):
        if(id not in self.m_aIndex):
            return {'success': False}
        
        hFile = self.m_hFile
        
        r = {'success': True}
        r['id'] = id
        r['fields'] = {}
        
        hFile.seek(self.m_aIndex[ id ]['start'])
        h = struct.unpack('<lhhhhhhh', hFile.read(18))
        
        offset = 6*h[6]
        hFile.seek(offset, 1)
        
        r['str'] = hFile.read(abs(h[1])-h[5]).decode(self.m_sEncoding)
        
        offset += abs(h[1])-h[5]
        hFile.seek(-offset, 1)
        
        for i in range(0, h[6]):
            h = struct.unpack('<hhh', hFile.read(6))
            field = str(h[0])
            
            if field not in r['fields']:
                r['fields'][ field ] = []
            text = r['str'][ h[1]:h[1]+h[2] ]
            text = re.split('\^([a-z0-9])', text, flags=re.IGNORECASE)
            
            #if (text[0] != '^'):
            if (len(text) == 1):
                r['fields'][ field ].append(text[0])
                continue
            r['fields'][ field ].append({})
            
            for j in range(1, len(text)):
                if(j % 2 == 0):
                    continue
                r['fields'][ field ][ len(r['fields'][ field ])-1 ][ text[j] ] = text[j+1]
            
        if (wstr):
            r['str'].encode('utf-8')
        else:
            r.pop('str')
        
        return r
    
    def ReadField(self, row, field, sfield = None):
        if ('fields' not in row):
            return None
        if (field not in row['fields']):
            return None
        if (sfield == None):
            return row['fields'][field]
        if (len(row['fields'][field]) == 0):
            return None
        if (sfield not in row['fields'][field][0]):
            return None
        return row['fields'][field][0][sfield]
    
    def Count(self, bAll = False):
        return self.m_iRecords[0] if bAll else self.m_iRecords[1]
    
    
    
    
    
    
    
    
    