import pandas
import matplotlib.pyplot as plt

df = pandas.read_csv('db\svk.csv', header=True, delimiter=';', quotechar='"', names=['id', 'type', 'name', 'author', 'phouse', 'year', 'ccount', 'isbn'])

#pandas.to_datetime(df.year, format='%Y')
df.ccount.plot()
plt.show()