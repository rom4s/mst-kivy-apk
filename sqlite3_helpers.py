# coding=utf8

import re

# Переопределение оператора сравнения с двумя и тремя параметрами
def sqlite_like(template_, value_):
    return sqlite_like_escape(template_, value_, None)

def sqlite_like_escape(template_, value_, escape_):
    re_ = re.compile(template_.lower().
                        replace(".", "\\.").replace("^", "\\^").replace("$", "\\$").
                        replace("*", "\\*").replace("+", "\\+").replace("?", "\\?").
                        replace("{", "\\{").replace("}", "\\}").replace("(", "\\(").
                        replace(")", "\\)").replace("[", "\\[").replace("]", "\\]").
                        replace("_", ".").replace("%", ".*?"))
    return re_.match(value_.lower()) != None    
    
# Переопределение функции преобразования к нижнему регистру
def sqlite_lower(value_):
    return value_.lower()
      
# Переопределение правила сравнения строк
def sqlite_nocase_collation(value1_, value2_):
    return cmp(value1_.decode('utf-8').lower(), value2_.decode('utf-8').lower())
  
# Переопределение функции преобразования к верхнему геристру
def sqlite_upper(value_):
    return value_.upper()